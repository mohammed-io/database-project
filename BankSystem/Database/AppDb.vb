﻿Imports System.Data.Entity

Public Class AppDb
    Inherits DbContext

    Public Property Roles As DbSet(Of Role)
    Public Property Accounts As DbSet(Of Account)
    Public Property Transactions As DbSet(Of Transaction)

    Public Sub New()
        MyBase.New("Data Source=(LocalDB)\MSSQLLocalDB; Database=BankSystemDb")

    End Sub

    Protected Overrides Sub OnModelCreating(modelBuilder As DbModelBuilder)

        modelBuilder.ComplexType(Of Address)()


        modelBuilder.Entity(Of Account).HasMany(Function(x) x.ReceivedTransactions).WithRequired(Function(x) x.To)
        modelBuilder.Entity(Of Account).HasMany(Function(x) x.SentTransactions).WithRequired(Function(x) x.From)


        MyBase.OnModelCreating(modelBuilder)
    End Sub


    Shared _ctx As New AppDb()
    Public Shared Function GetDb() As AppDb
        Return _ctx
    End Function
End Class
