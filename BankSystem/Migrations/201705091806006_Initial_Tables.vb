Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class Initial_Tables
        Inherits DbMigration
    
        Public Overrides Sub Up()
            CreateTable(
                "dbo.Accounts",
                Function(c) New With
                    {
                        .Id = c.Int(nullable := False, identity := True),
                        .Username = c.String(),
                        .FirstName = c.String(),
                        .LastName = c.String(),
                        .Email = c.String(),
                        .PhoneNumber = c.String(),
                        .Balance = c.Decimal(nullable := False, precision := 18, scale := 2),
                        .Address_City = c.String(),
                        .Address_Country = c.String(),
                        .Address_ZipCode = c.String(maxLength := 5),
                        .Address_Street = c.String(),
                        .RoleId = c.Int(nullable := False)
                    }) _
                .PrimaryKey(Function(t) t.Id) _
                .ForeignKey("dbo.Roles", Function(t) t.RoleId, cascadeDelete := True) _
                .Index(Function(t) t.RoleId)

            CreateTable(
                "dbo.Transactions",
                Function(c) New With
                    {
                        .Id = c.Int(nullable:=False, identity:=True),
                        .Amount = c.Decimal(nullable:=False, precision:=18, scale:=2),
                        .ToId = c.Int(nullable:=False),
                        .FromId = c.Int(nullable:=False)
                    }) _
                .PrimaryKey(Function(t) t.Id) _
                .ForeignKey("dbo.Accounts", Function(t) t.ToId, cascadeDelete:=False) _
                .ForeignKey("dbo.Accounts", Function(t) t.FromId, cascadeDelete:=False) _
                .Index(Function(t) t.ToId) _
                .Index(Function(t) t.FromId)

            CreateTable(
                "dbo.Roles",
                Function(c) New With
                    {
                        .Id = c.Int(nullable := False, identity := True),
                        .Name = c.String(),
                        .Rank = c.Byte(nullable := False)
                    }) _
                .PrimaryKey(Function(t) t.Id)
            
        End Sub
        
        Public Overrides Sub Down()
            DropForeignKey("dbo.Accounts", "RoleId", "dbo.Roles")
            DropForeignKey("dbo.Transactions", "FromId", "dbo.Accounts")
            DropForeignKey("dbo.Transactions", "ToId", "dbo.Accounts")
            DropIndex("dbo.Transactions", New String() { "FromId" })
            DropIndex("dbo.Transactions", New String() { "ToId" })
            DropIndex("dbo.Accounts", New String() { "RoleId" })
            DropTable("dbo.Roles")
            DropTable("dbo.Transactions")
            DropTable("dbo.Accounts")
        End Sub
    End Class
End Namespace
