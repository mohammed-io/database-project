Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class Added_Password_Property_to_Account
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Accounts", "Password", Function(c) c.String())
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.Accounts", "Password")
        End Sub
    End Class
End Namespace
