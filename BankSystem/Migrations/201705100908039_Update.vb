Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class Update
        Inherits DbMigration
    
        Public Overrides Sub Up()
            RenameColumn(table := "dbo.Transactions", name := "FromId", newName := "__mig_tmp__0")
            RenameColumn(table := "dbo.Transactions", name := "ToId", newName := "FromId")
            RenameColumn(table := "dbo.Transactions", name := "__mig_tmp__0", newName := "ToId")
            RenameIndex(table := "dbo.Transactions", name := "IX_FromId", newName := "__mig_tmp__0")
            RenameIndex(table := "dbo.Transactions", name := "IX_ToId", newName := "IX_FromId")
            RenameIndex(table := "dbo.Transactions", name := "__mig_tmp__0", newName := "IX_ToId")
        End Sub
        
        Public Overrides Sub Down()
            RenameIndex(table := "dbo.Transactions", name := "IX_ToId", newName := "__mig_tmp__0")
            RenameIndex(table := "dbo.Transactions", name := "IX_FromId", newName := "IX_ToId")
            RenameIndex(table := "dbo.Transactions", name := "__mig_tmp__0", newName := "IX_FromId")
            RenameColumn(table := "dbo.Transactions", name := "ToId", newName := "__mig_tmp__0")
            RenameColumn(table := "dbo.Transactions", name := "FromId", newName := "ToId")
            RenameColumn(table := "dbo.Transactions", name := "__mig_tmp__0", newName := "FromId")
        End Sub
    End Class
End Namespace
