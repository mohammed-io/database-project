﻿Public Class AdminWindow
    Dim ctx As AppDb = AppDb.GetDb()

    Dim acc As Account

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        UpdateBindings()

    End Sub

    Public Sub UpdateBindings()
        TransactionsDataGrid.ItemsSource = ctx.Transactions.ToList()
        UsersDataGrid.ItemsSource = ctx.Accounts.ToList()
    End Sub


    Private Sub AddButton_Click(sender As Object, e As RoutedEventArgs)

        Try
            Dim editWin As New CreateUserWindow(New Account)

            editWin.ShowDialog()

            UpdateBindings()
        Catch ex As Exception
            MessageBox.Show(ex.Message)

        End Try

    End Sub

    Private Sub EditButton_Click(sender As Object, e As RoutedEventArgs)

        Try
            Dim editWin As New EditUserWindow(acc)

            editWin.ShowDialog()

            UpdateBindings()

        Catch ex As Exception
            MessageBox.Show(ex.Message)

        End Try

    End Sub

    Private Sub DeleteButton_Click(sender As Object, e As RoutedEventArgs)

        Try

            ctx.Accounts.Remove(acc)

            ctx.SaveChanges()

            UpdateBindings()
        Catch ex As Exception
            MessageBox.Show(ex.Message)

        End Try

    End Sub

    Private Sub DataGrid_Selected(sender As Object, e As RoutedEventArgs)

    End Sub

    Private Sub DataGrid_SelectedCellsChanged(sender As Object, e As SelectedCellsChangedEventArgs)
        acc = TryCast(UsersDataGrid.SelectedItem, Account)
    End Sub
End Class
