﻿Public Class TransferWindow
    Dim _trans As Transaction

    Public Sub New(trans As Transaction)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        _trans = trans
        DataContext = trans
    End Sub

    Private Sub TransferButton_Click(sender As Object, e As RoutedEventArgs)
        Dim ctx = AppDb.GetDb()

        Try
            Dim destination = ctx.Accounts.First(Function(x) x.Id = _trans.ToId)
            Dim source = ctx.Accounts.First(Function(x) x.Id = Authentication.CurrentUser.Id)

            source.Balance -= _trans.Amount
            destination.Balance += _trans.Amount

            _trans.To = destination
            _trans.From = source

            ctx.Transactions.Add(_trans)

            ctx.SaveChanges()

            Close()

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub CancelButton_Click(sender As Object, e As RoutedEventArgs)
        Close()
    End Sub
End Class
