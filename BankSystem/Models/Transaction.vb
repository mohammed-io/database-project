﻿Imports System.ComponentModel.DataAnnotations.Schema

Public Class Transaction
    Public Property Id As Integer
    Public Property Amount As Decimal

    Public Property ToId As Integer
    <ForeignKey("ToId")>
    Public Overridable Property [To] As Account

    Public Property FromId As Integer
    <ForeignKey("FromId")>
    Public Overridable Property From As Account
End Class
