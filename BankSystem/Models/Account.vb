﻿Imports System.Collections.ObjectModel
Imports System.ComponentModel.DataAnnotations.Schema

Public Class Account
    Public Property Id As Integer
    Public Property Username As String
    Public Property FirstName As String
    Public Property LastName As String
    Public Property Email As String
    Public Property PhoneNumber As String
    Public Property Balance As Decimal
    Public Property Address As Address = New Address()
    Public Property RoleId As Integer
    Public Overridable Property Role As Role

    Public Property Password As String

    Public Overridable Property ReceivedTransactions As List(Of Transaction) = New List(Of Transaction)
    Public Overridable Property SentTransactions As List(Of Transaction) = New List(Of Transaction)

    <NotMapped>
    Public ReadOnly Property Transactions As ObservableCollection(Of Transaction)
        Get
            Return New ObservableCollection(Of Transaction)(ReceivedTransactions _
                .Concat(SentTransactions) _
                .OrderBy(Function(x) x.Id))
        End Get
    End Property


    Public Overrides Function ToString() As String
        Return Username
    End Function
End Class
