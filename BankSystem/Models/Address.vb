﻿Imports System.ComponentModel.DataAnnotations

Public Class Address

    Public Property City As String
    Public Property Country As String
    <MaxLength(5), MinLength(5)>
    Public Property ZipCode As String
    Public Property Street As String
End Class
