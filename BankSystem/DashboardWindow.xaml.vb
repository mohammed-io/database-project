﻿Public Class DashboardWindow
    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()
        ' Add any initialization after the InitializeComponent() call.

        DataContext = Authentication.CurrentUser
        TransactionsDataGrid.ItemsSource = Authentication.CurrentUser.Transactions

        AdminDashboardMenuItem.Visibility = If(Authentication.CurrentUser.Role?.Name.ToLower() = "admin", Visibility.Visible, Visibility.Collapsed)
    End Sub

    Private Sub ExitMenuITem_Click(sender As Object, e As RoutedEventArgs)
        Close()
    End Sub

    Private Sub AdminDashboardMenuItem_Click(sender As Object, e As RoutedEventArgs)
        Dim adminWindow As New AdminWindow()

        adminWindow.ShowDialog()
    End Sub

    Private Sub TransferButton_Click(sender As Object, e As RoutedEventArgs)

        Dim win As New TransferWindow(New Transaction)

        win.ShowDialog()

        DataContext = New Account
        DataContext = Authentication.CurrentUser
        TransactionsDataGrid.ItemsSource = Authentication.CurrentUser.Transactions
    End Sub
End Class
