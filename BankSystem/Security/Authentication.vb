﻿Public Class Authentication
    Private Shared _currentUser As Account

    Public Shared Property CurrentUser As Account
        Get
            Return _currentUser
        End Get
        Private Set(value As Account)
            _currentUser = value
        End Set
    End Property


    Public Shared Function Authenticate(username As String, password As String) As Boolean

        Dim ctx = AppDb.GetDb()

        CurrentUser = ctx.Accounts.ToList() _
                        .SingleOrDefault(Function(u) u.Username = username And u.Password = password)

        Return Not (CurrentUser Is Nothing)
    End Function

End Class
