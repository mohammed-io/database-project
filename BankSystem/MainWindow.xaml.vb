﻿Class MainWindow

    Private Sub LoginButton_Click(sender As Object, e As RoutedEventArgs)
        If (Authentication.Authenticate(UsernameTextBox.Text, PasswordPasswordBox.Password)) Then
            Dim win As New DashboardWindow

            Application.Current.MainWindow = win

            win.Show()

            Me.Close()
        Else
            MessageBox.Show("Authentication failed!", "Failure", MessageBoxButton.OK, MessageBoxImage.Error)
        End If
    End Sub

    Private Sub CancelButton_Click(sender As Object, e As RoutedEventArgs)
        Close()
    End Sub

    Private Sub RegisterButton_Click(sender As Object, e As RoutedEventArgs)
        Dim win As New CreateUserWindow(New Account)

        win.ShowDialog()
    End Sub
End Class
