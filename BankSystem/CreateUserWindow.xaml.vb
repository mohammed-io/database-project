﻿Public Class CreateUserWindow
    Dim ctx As AppDb = AppDb.GetDb()
    Dim _acc As Account

    Public Sub New(acc As Account)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        _acc = acc

        DataContext = acc
    End Sub

    Private Sub SaveButton_Click(sender As Object, e As RoutedEventArgs)
        ctx.Accounts.Add(_acc)
        _acc.RoleId = 2

        Try
            ctx.SaveChanges()

            Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub CancelButton_Click(sender As Object, e As RoutedEventArgs)
        Close()
    End Sub
End Class
