﻿Public Class EditUserWindow
    Dim _acc As Account
    Dim ctx As AppDb = AppDb.GetDb()

    Public Sub New(acc As Account)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        _acc = ctx.Accounts.FirstOrDefault(Function(x) x.Id = acc.Id)

        DataContext = _acc
    End Sub

    Private Sub SaveButton_Click(sender As Object, e As RoutedEventArgs)

        Try
            ctx.SaveChanges()

            Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub CancelButton_Click(sender As Object, e As RoutedEventArgs)
        Close()
    End Sub
End Class
