﻿Public Class Form1
    Dim bold As FontStyle = FontStyle.Regular
    Dim italic As FontStyle = FontStyle.Regular
    Dim underline As FontStyle = FontStyle.Regular
    Dim strike As FontStyle = FontStyle.Regular

    Private Sub UpdateFont()
        Label1.Font = New Font(Label1.Font, bold Or italic Or underline Or strike)

    End Sub

    Private Sub BoldCheckBox_CheckedChanged(sender As Object, e As EventArgs) Handles BoldCheckBox.CheckedChanged
        bold = If(BoldCheckBox.Checked, FontStyle.Bold, FontStyle.Regular)

        UpdateFont()
    End Sub

    Private Sub ItalicCheckBox_CheckedChanged(sender As Object, e As EventArgs) Handles ItalicCheckBox.CheckedChanged
        italic = If(ItalicCheckBox.Checked, FontStyle.Italic, FontStyle.Regular)
        UpdateFont()

    End Sub

    Private Sub UnderlineCheckBox_CheckedChanged(sender As Object, e As EventArgs) Handles UnderlineCheckBox.CheckedChanged
        underline = If(UnderlineCheckBox.Checked, FontStyle.Underline, FontStyle.Regular)
        UpdateFont()

    End Sub

    Private Sub StrikeoutCheckBox_CheckedChanged(sender As Object, e As EventArgs) Handles StrikeoutCheckBox.CheckedChanged
        strike = If(StrikeoutCheckBox.Checked, FontStyle.Strikeout, FontStyle.Regular)
        UpdateFont()

    End Sub
End Class
