﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.BoldCheckBox = New System.Windows.Forms.CheckBox()
        Me.ItalicCheckBox = New System.Windows.Forms.CheckBox()
        Me.UnderlineCheckBox = New System.Windows.Forms.CheckBox()
        Me.StrikeoutCheckBox = New System.Windows.Forms.CheckBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'BoldCheckBox
        '
        Me.BoldCheckBox.AutoSize = True
        Me.BoldCheckBox.Location = New System.Drawing.Point(51, 105)
        Me.BoldCheckBox.Name = "BoldCheckBox"
        Me.BoldCheckBox.Size = New System.Drawing.Size(46, 17)
        Me.BoldCheckBox.TabIndex = 0
        Me.BoldCheckBox.Text = "Bold"
        Me.BoldCheckBox.UseVisualStyleBackColor = True
        '
        'ItalicCheckBox
        '
        Me.ItalicCheckBox.AutoSize = True
        Me.ItalicCheckBox.Location = New System.Drawing.Point(51, 151)
        Me.ItalicCheckBox.Name = "ItalicCheckBox"
        Me.ItalicCheckBox.Size = New System.Drawing.Size(49, 17)
        Me.ItalicCheckBox.TabIndex = 1
        Me.ItalicCheckBox.Text = "Italic"
        Me.ItalicCheckBox.UseVisualStyleBackColor = True
        '
        'UnderlineCheckBox
        '
        Me.UnderlineCheckBox.AutoSize = True
        Me.UnderlineCheckBox.Location = New System.Drawing.Point(51, 186)
        Me.UnderlineCheckBox.Name = "UnderlineCheckBox"
        Me.UnderlineCheckBox.Size = New System.Drawing.Size(71, 17)
        Me.UnderlineCheckBox.TabIndex = 2
        Me.UnderlineCheckBox.Text = "Underline"
        Me.UnderlineCheckBox.UseVisualStyleBackColor = True
        '
        'StrikeoutCheckBox
        '
        Me.StrikeoutCheckBox.AutoSize = True
        Me.StrikeoutCheckBox.Location = New System.Drawing.Point(51, 224)
        Me.StrikeoutCheckBox.Name = "StrikeoutCheckBox"
        Me.StrikeoutCheckBox.Size = New System.Drawing.Size(102, 17)
        Me.StrikeoutCheckBox.TabIndex = 3
        Me.StrikeoutCheckBox.Text = "Strike Thruough"
        Me.StrikeoutCheckBox.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(104, 36)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(38, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Label1"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(398, 326)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.StrikeoutCheckBox)
        Me.Controls.Add(Me.UnderlineCheckBox)
        Me.Controls.Add(Me.ItalicCheckBox)
        Me.Controls.Add(Me.BoldCheckBox)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents BoldCheckBox As CheckBox
    Friend WithEvents ItalicCheckBox As CheckBox
    Friend WithEvents UnderlineCheckBox As CheckBox
    Friend WithEvents StrikeoutCheckBox As CheckBox
    Friend WithEvents Label1 As Label
End Class
